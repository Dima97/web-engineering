<?PHP
    $jsonFileName = __DIR__ . "/../json/comments.json";
    $logFileName = __DIR__ . "/../json/comments.log";
    file_put_contents($logFileName, "############################################\n", FILE_APPEND | LOCK_EX );

    if (isset($_POST["comment"])) {
        $name = $_POST["name"];
        $comment = $_POST["comment"];

        file_put_contents($logFileName, "Adding Comment:\nname: " . $name . "\ncomment: " . $comment . "\n", FILE_APPEND | LOCK_EX );

        $file = file_get_contents($jsonFileName);
        $allComments = json_decode($file);
        $data = array("name" => $name, "comment" => $comment);

        file_put_contents($logFileName, "Generated Array:\n" . print_r($data, true) . "\n", FILE_APPEND | LOCK_EX );
        $allComments[] = $data;

        if (file_put_contents($jsonFileName, json_encode($allComments), LOCK_EX)) {
            file_put_contents($logFileName, "Added " . json_encode($allComments) . " to " . $jsonFileName . "\n", FILE_APPEND | LOCK_EX );
        } else {
            file_put_contents($logFileName, "Sending Comment failed! Could not put data to JSON-File!\n", FILE_APPEND | LOCK_EX );
        }
    } else {
        file_put_contents($logFileName, "Sending Comment failed! Could not recieve data!\n", FILE_APPEND | LOCK_EX );
    }
?>