let file;
let aside;
let main;
let link;

(async (_) => {
    const url = "http://www2.inf.h-brs.de/~dreima2s/solutions/async/www.json";
    file = await fetchJSON(url);
    console.log(file);

    aside = document.getElementById("as");
    main = document.getElementById("ma");
    link = document.getElementById("link");
})();

async function fetchJSON(url) {
    const request = await fetch(url);
    return await request.json();
}

async function onHTML() {
    console.log("Opening Topic HTML");
    load("html");
}

async function onCSS() {
    console.log("Opening Topic CSS");
    load("css");
}

async function onJS() {
    console.log("Opening Topic JavaScript");
    load("js");
}

async function load(name) {
    const json = await getJSONObj(name);
    console.log(json);

    // removing all children
    aside.textContent = "";

    for (const entry of Object.keys(json)) {
        const btn = document.createElement("button");
        btn.classList.add("btn_aside");

        const node = json[entry];
        btn.textContent = entry;

        btn.onclick = function() {
            console.log("Reading:");
            console.log(entry);
            console.log("Content: " + node.content);
            console.log("Ref:" + node.references);

            main.textContent = node.content;
            link.textContent = node.references;
            link.setAttribute("href", node.references);
        };

        aside.appendChild(btn);
        console.log("Added button: " + entry);
    }
}

async function getJSONObj(name) {
    if (name == "html") {
        return file.html;
    } else if (name == "css") {
        return file.css;
    } else {
        return file.javascript;
    }
}
