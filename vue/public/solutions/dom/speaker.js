let current_interval = null, current_button = null, current_timer = null;

window.onload = function () {
    const button = document.getElementById("add_btn");
    const input = document.getElementById("add_value");
    const list = document.getElementById("speakers");

    if (button || input || list) {
        button.addEventListener("click", (event) => {
            const name = input.value;
            if (name != "") {
                console.log("adding new speaker: " + name);

                const speaker = document.createElement("li");
                const table = document.createElement("table");
                const row = document.createElement("tr");

                const name_cell = document.createElement("td");
                const time_cell = document.createElement("td");
                const btn_cell = document.createElement("td");

                const btn = document.createElement("button");

                btn_cell.appendChild(btn);
                row.appendChild(name_cell);
                row.appendChild(time_cell);
                row.appendChild(btn_cell);
                table.appendChild(row);
                speaker.appendChild(table);

                name_cell.innerText = name;
                time_cell.innerText = "00:00:00";
                btn.innerText = "Stopp!";

                console.log("starting Timer");
                let timer = Timer(time_cell);
                let interval = setInterval(timer, 1000);
                btn.onclick = stopTimer(interval, timer, btn);

                stopAllTimers(interval, timer, btn);
                list.appendChild(speaker);
            } else {
                console.error("Name is empty!!!");
            }
        });
    } else {
        if (!button) {
            console.error("Could not get the submit button");
        }

        if (!input) {
            console.error("Could not get the input field");
        }

        if (!list) {
            console.error("Could not get the speaker list");
        }
    }
};

function Timer(cell) {
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    function run() {
        if (seconds == 59) {
            seconds = 0;
            addM();
        } else {
            ++seconds;
        }

        updateHTML();
    }

    function addM() {
        if (minutes == 59) {
            minutes = 0;
            ++hours;
        } else {
            ++minutes;
        }
    }

    function updateHTML() {
        const text = formatNumber(hours) + ":" + formatNumber(minutes) + ":" + formatNumber(seconds);
        cell.innerText = text;
    }

    function formatNumber(number) {
        return ("0" + number).slice(-2);
    }

    function getButton() {
        return button;
    }

    return run;
}

function stopTimer(interval, timer, button) {
    return function () {
        console.log("stopping Timer");
        clearInterval(interval);
        
        button.innerText = "Start!";
        button.onclick = restartTimer(interval, timer, button);

        current_interval = null;
        current_button = null;
        current_timer = null;
    };
}

function restartTimer(interval, timer, button) {
    return function () {
        console.log("restarting Timer");
        interval = setInterval(timer, 1000);
        stopAllTimers(interval, timer, button);

        button.innerText = "Stopp!";
        button.onclick = stopTimer(interval, timer, button);
    };
}

function stopAllTimers(interval, timer, button) {
    if (current_interval != null && current_timer != null && current_button != null) {
        stopTimer(current_interval, current_timer, current_button)();
    } else {
        console.warn("Couldn't close all timers!!");
    }

    current_interval = interval;
    current_timer = timer;
    current_button = button;
}
