var Car = {
    name = "car"
};

var Person = {
    car : Car
};

function conflict(searchedCar) {
    var curr = Person, numCars = 0;

    do {
        var currPerson = Object.getOwnPropertyNames(curr);
        console.log(currPerson.car);
        if (currPerson.car === searchedCar) {
            ++numCars;
        };
    } while (curr = Object.getPrototypeOf(curr))
    
    if (numCars > 1) {
        return true;
    } else {
        return false;
    }
}

var Hans = Object.create(Person);
Hans.car = "BMW";

var Peter = Object.create(Person);
Peter.car = "VW";

var Günter = Object.create(Person);
Günter.car = "Audi";

var Tim = Object.create(Person);
Tim.car = "BMW";

console.log(conflict("BMW"));
console.log(conflict("VW"));
console.log(conflict("Audi"));