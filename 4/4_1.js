function identity(x) {
    return x;
}

function identity_function(x) {
    return function () {
        return x;
    };
}

function add(x, y) {
    return x + y;
}

function mul(x, y) {
    return x * y;
}

function add(x) {
    return function (y) {
        return x + y;
    };
}

function applyf(fun) {
    return function (x) {
        return function (y) {
            return fun(x, y);
        };
    };
}